package vaarsuvius;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileDeleteStrategy;

public class Familicide {

	private static final String TARGET = "target";
	private static final String KILLABLE = "killable";
	private static final String PRESERVE = "preserve";

	public static void main(String[] args) {

		Options options = new Options();

		Option target = new Option("t", TARGET, true, "file or directory to force delete / kill locking applications");
		target.setRequired(true);
		options.addOption(target);
		
		Option lockApplication = new Option("k", KILLABLE, true, "comma seperated white list of valid application names to kill");
		lockApplication.setRequired(true);
		lockApplication.setValueSeparator(',');
		options.addOption(lockApplication);
		
		Option preserve = new Option("p", PRESERVE, false, "flag to indicate the directory should not be deleted. Just kill applications used by the directory");
		options.addOption(preserve);
		
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd = null;

		try {
			cmd = parser.parse(options, args, true);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("java -jar familicide.jar <args>", options);

			System.exit(1);
		}

		String targetName = cmd.getOptionValue(TARGET);
				
		Set<String> killableApplicationSet = new HashSet<>();
		if(cmd.getOptionValues(KILLABLE) != null)
			killableApplicationSet = new HashSet<>(Arrays.asList(cmd.getOptionValue(KILLABLE).split(",")));

		System.out.println("Attempting to delete " + targetName + " and all children");

		if(!killableApplicationSet.isEmpty())
			findLockingProcesses(targetName, killableApplicationSet).forEach(processId -> killProcess(processId));

		File targetFile = new File(targetName);

		try {
			if(!cmd.hasOption(PRESERVE))
				recursiveFileDelete(targetFile);
		} catch (IOException e) {
			System.out.println(e.getMessage());

			System.exit(1);
		}
		System.out.println("It is done");

	}

	private static void recursiveFileDelete(File fileToDelete) throws IOException {
		if (fileToDelete.isDirectory()) {
			for (File childFile : fileToDelete.listFiles()) {
				recursiveFileDelete(childFile);
			}
		}

		fileDelete(fileToDelete);
	}

	private static void fileDelete(File fileToDelete) throws IOException {
		System.out.println("deleting " + fileToDelete);
		FileDeleteStrategy.FORCE.delete(fileToDelete);

	}

	private static Set<String> findLockingProcesses(String fileName, Set<String> killableApplicationSet) {
		Set<String> killableLockingProcesses = new HashSet<>();
		Set<String> unkillableLockingProcesses = new HashSet<>();
		try {
			String line;
			Process p = Runtime.getRuntime().exec("openfiles /query /v /nh /fo csv");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

			while ((line = input.readLine()) != null) {
				String[] tokens = line.split(",");

				if (tokens.length == 5 && tokens[4].contains(clean(fileName))) {
					if (!killableApplicationSet.isEmpty() && killableApplicationSet.contains(clean(tokens[3]))) {
						killableLockingProcesses.add(clean(tokens[2]));
					} else {
						unkillableLockingProcesses.add(clean(tokens[2])+"::"+clean(tokens[3]));
					}

				}
			}

			input.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		killableApplicationSet.forEach((app)->System.out.println("Killable Applications :"+app));
		killableLockingProcesses.forEach((pid)->System.out.println("Killable Locking PID :"+pid));
		unkillableLockingProcesses.forEach((tag)->System.out.println("Unkillable Locking PID::PROCESS :"+tag));

		return killableLockingProcesses;
	}

	private static void killProcess(String processId) {
		System.out.println("Killing process with PID :" + processId.replace("\"", ""));
		try {
			Runtime.getRuntime().exec("taskkill /F /PID " + processId.replace("\"", "")).waitFor();
		} catch (IOException | InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private static String clean(String clean) {
		return clean.trim().replace("\"", "").trim();
	}

}
